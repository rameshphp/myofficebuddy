function validateLogin(){
    //
    var email = getInputData('email');
    var pwd = getInputData('password');

    if(email && pwd) {
        var username = '';
        resetInputControl('email');
        if(email == 'peter' && pwd == 'password') {
            username = 'Peter';
            window.location.href = '/myofficebuddy.html?usn=Peter';
        }
        else if(email == 'rahul' && pwd == 'password') {
            username = 'Rahul';
            window.location.href = '/myofficebuddy.html?usn=Rahul';
        }
        else {
            $("#parsley-id-email").show();
            $("#email").addClass('parsley-error');
            $('#parsley-id-email li').text('Invalid credentials.');
        }
    }
}

function getInputData(id){
    var eleValue = $.trim($("#" + id).val());
    
    if(eleValue){
        resetInputControl(id);
        return eleValue;
    }
    else {
        $('#parsley-id-email li').text('This value is required.');
        $("#parsley-id-" + id).show();
        $("#" + id).addClass('parsley-error');
    }
}

function resetInputControl(id) {
    $("#parsley-id-" + id).hide();
    $("#" + id).removeClass('parsley-error');
}

