/* eslint-disable */

import React, { Component } from 'react';
import { Login } from './login';
import { MyOfficeBuddy } from './ChatBot';

export class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: ''
        }
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }

    componentWillMount() {
        this.setState({ content: <Login login={this.login} /> });
        if (sessionStorage.IsLoggedIn == 'true') {
            this.setState({ content: <MyOfficeBuddy logout={this.logout} /> });
        }
    }

    login() {
        sessionStorage.setItem('IsLoggedIn', true);
        if(document.getElementById('email')&& document.getElementById('email')!==''){
            sessionStorage.setItem('UserName',document.getElementById('email').value);
        }
        if (sessionStorage.IsLoggedIn == 'true') {
            this.setState({ content: <MyOfficeBuddy logout={this.logout} /> });
        }
    }

    logout() {
        sessionStorage.clear();
        this.setState({ content: <Login login={this.login} /> });
    }

    render() {

        return (
            <div>{this.state.content}</div>
        )
    }
}

export default App;

