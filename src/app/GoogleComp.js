/* eslint-disable */

import React, { Component } from 'react';
//import gapi from 'gapi-client';
import request from 'superagent';

export class GoogleComp extends Component {
  constructor(props) {
    super(props);
    this.triggerNext = this.triggerNext.bind(this);
    this.applyLeave = this.applyLeave.bind(this);
    this.converDate = this.convertDate.bind(this);
  }

  componentWillMount() {
    let _this = this;
    //gapi.load('client:auth2', this.initGapi());
    const script = document.createElement("script");
    script.src = "https://apis.google.com/js/api.js";

    script.onload = () => {
      gapi.load('client', () => {
        gapi.client.init({
          apiKey: 'AIzaSyBkJxFPXLnph8erRg7Fq3ydfsnlO2qB2r0',
          //discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
          clientId: '184066041958-1ukjaall998s4i292bqf9sc13hojb9j1.apps.googleusercontent.com',
          scope: 'https://www.googleapis.com/auth/calendar'
        }).then(function () {
          if (gapi.auth2.getAuthInstance().isSignedIn.get() == true) {
            _this.applyLeave();
          } else {
            gapi.auth2.getAuthInstance().signIn().then(function (res) {
              _this.applyLeave();
            }, function (error) {
            });
          }
        });
      });
    };

    document.body.appendChild(script);
  }

  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
  }

  applyLeave() {
    let _this = this;
    let startDate, endDate;
    if (this.props.steps[18].message == 'Today') {
      if (this.props.steps[18] && this.props.steps[18].value !== undefined) {
        startDate = this.props.steps[18].value;
      }
    } else {
      if (this.props.steps.customFromDateUser && this.props.steps.customFromDateUser.value !== undefined)
        startDate = this.props.steps.customFromDateUser.value;
    }

    if (this.props.previousStep && this.props.previousStep.value !== undefined) {
      let dateVal = new Date(this.props.previousStep.value);
      dateVal.setDate(dateVal.getDate() + 1);
      endDate = this.convertDate(dateVal);
    }

    if (startDate !== undefined && endDate !== undefined) {

      let resource = {
        "summary": "Leave",
        "start": {
          "date": startDate
        },
        "end": {
          "date": endDate
        }
      };

      gapi.client.request({
        'path': 'https://www.googleapis.com/calendar/v3/users/me/calendarList',
      }).then(function (response) {
        if (response.result.items.length > 0) {
          let calendarId = response.result.items[0].id;
          gapi.client.request({
            'path': 'https://www.googleapis.com/calendar/v3/calendars/' + calendarId + '/events',
            'method': 'POST',
            'body': resource
          }).then(function (response) {
            _this.triggerNext('23');
          }, function (reason) {
            _this.triggerNext('LeaveApplyError');
          });
        }
      }, function (reason) {
        _this.triggerNext('LeaveApplyError');
      });
    } else {
      _this.triggerNext('LeaveApplyError');
    }



    //   let calendarId = 'unrealimitators@gmail.com';

    //   gapi.client.request({
    //     'path': 'https://www.googleapis.com/calendar/v3/calendars/' + calendarId + '/events',
    //     'method': 'POST',
    //     'body': resource
    //   }).then(function (response) {
    //     _this.triggerNext('23');
    //   }, function (reason) {
    //   });
  }

  triggerNext(val) {
    this.props.triggerNextStep({ trigger: val });
  }

  render() {
    return (
      <div ></div >
    )
  }
}

export default GoogleComp;

