/* eslint-disable */

import React, { Component } from 'react';
import request from 'superagent';

export class ApiAi extends Component {
  constructor(props) {
    super(props);
    this.triggerNext = this.triggerNext.bind(this);
  }

  componentWillMount() {
    let _this = this;

    request
      .get('https://api.api.ai/v1/query?v=20150910&lang=en&sessionId=1234567890&query=' + this.props.previousStep.message)
      .set('Authorization', 'Bearer c7c89437ab3c4d1680d62694f79aec0f')
      .end(function (err, res) {
        if (res.body.result.action == 'agent.actions') {
          _this.triggerNext('5');
        } else if (res.body.result.action == 'leave.apply') {
          _this.triggerNext('9');
        } else if (res.body.result.action == 'project.status') {
          _this.triggerNext('projectStatusTitle');
        } else if (res.body.result.action == 'project.risk') {
          _this.triggerNext('projectRiskTitle');
        } else if (res.body.result.action == 'project.risk.resourcepool') {
          _this.triggerNext('projectRiskResourcePool');
        } else if (res.body.result.action == 'project.risk.notify') {
          _this.triggerNext('projectRiskResourceNotify');
        } else if (res.body.result.action == 'schedule') {
          _this.triggerNext('scheduleTitle');
        } else if (res.body.result.action == 'input.thank') {
          _this.triggerNext('WelcomeText');
        } else if (res.body.result.action == 'input.welcome') {
          _this.triggerNext('2');
        } else {
          _this.triggerNext('Unknown');
        }
      });

  }

  triggerNext(val) {
    this.props.triggerNextStep({ trigger: val });
  }

  render() {
    return (
      <div></div>
    )
  }
}

export default ApiAi;

