/* eslint-disable */
import React, { Component } from 'react';
import ApiAi from './ApiAi';
import GoogleComp from './GoogleComp';

function convertDate(date) {
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth() + 1).toString();
  var dd = date.getDate().toString();

  var mmChars = mm.split('');
  var ddChars = dd.split('');

  return yyyy + '-' + (mmChars[1] ? mm : "0" + mmChars[0]) + '-' + (ddChars[1] ? dd : "0" + ddChars[0]);
}

let today = new Date();
let tomorrow = new Date();
tomorrow.setDate(tomorrow.getDate() + 1);
today = convertDate(today);
tomorrow = convertDate(tomorrow);



export const Data = {
  steps: [
    {
      id: '1',
      message: 'Hello ',
      trigger: '3',
    },
    {
      id: '2',
      message: 'Welcome to my office buddy, How can I help you?',
      trigger: '3',
    },
    {
      id: '3',
      user: true,
      trigger: '4',
    },
    {
      id: '4',
      component: <ApiAi />,
      waitAction: true,
      replace: true,
    },
    {
      id: '5',
      message: 'I can assist you on the below, please choose one for your requirement',
      trigger: '6'
    },
    {
      id: '6',
      options: [
        { value: 1, label: 'Scheduling', trigger: '7' },
        { value: 2, label: 'Process and policy guidance', trigger: '7' },
        { value: 3, label: 'Project / Task Status Updates', trigger: 'projectStatus' },
        { value: 4, label: 'Risk Highlighter', trigger: '7' },
        { value: 5, label: 'Latest Business updates', trigger: '7' },
        { value: 6, label: 'Notifications', trigger: '7' },
        { value: 7, label: 'Stress Breaker', trigger: '7' },
      ],
    },
    {
      id: '7',
      message: 'How can I help you with Scheduling?',
      trigger: '8'
    },
    {
      id: '8',
      user: true,
      trigger: '4'
    },
    {
      id: '9',
      message: "Sorry, you are not eligible for it. Only female employees have that option. You can go for Paternity leave.",
      trigger: '10'
    },
    {
      id: '10',
      options: [
        { value: 1, label: 'What is Paternity Leave? ', trigger: '11' },
        { value: 2, label: 'Apply Paternity Leave', trigger: '17' },
        // { value: 3, label: 'Access Leave application ', trigger: '13' },
      ],
    },
    {
      id: '11',
      message: "Paternity leave is a type of leave to have for parenting activity, as per our records you can avail 5 days of Paternity leave in a year. ",
      trigger: '14'
    },
    {
      id: '14',
      options: [
        { value: 1, label: 'Apply Paternity Leave', trigger: '18' },
      ],
    },
    {
      id: '17',
      message: "Okay! From date?",
      trigger: '18'
    },
    {
      id: '18',
      options: [
        { value: today, label: 'Today', trigger: '19' },
        { value: 2, label: 'Custom Date', trigger: 'customFromDatetitle' },
      ],
    },
    {
      id: '19',
      message: "To Date?",
      trigger: '22'
    },
    {
      id: '22',
      options: [
        { value: today, label: 'Today', trigger: 'ApplyLeaveGoogle' },
        { value: tomorrow, label: 'Tomorrow', trigger: 'ApplyLeaveGoogle' },
        { value: 2, label: 'Custom Date', trigger: 'customDatetitle' },
      ],
    },
    {
      id: 'customDatetitle',
      message: "Please enter date(YYYY-mm-dd)",
      trigger: 'customDateUser'
    },
    {
      id: 'customDateUser',
      user: true,
      trigger: 'ApplyLeaveGoogle'
    },
    {
      id: 'customFromDatetitle',
      message: "Please enter from date(YYYY-mm-dd)",
      trigger: 'customFromDateUser'
    },
    {
      id: 'customFromDateUser',
      user: true,
      trigger: 'customToDatetitle'
    },
    {
      id: 'customToDatetitle',
      message: "Please enter To date(YYYY-mm-dd)",
      trigger: 'customToDateUser'
    },
    {
      id: 'customToDateUser',
      user: true,
      trigger: 'ApplyLeaveGoogle'
    },
    {
      id: 'ApplyLeaveGoogle',
      component: <GoogleComp />,
      waitAction: true,
      replace: true,
    },
    {
      id: '23',
      message: "Leave applied successfully",
      trigger: '24'
    },
    {
      id: '24',
      message: "Your available Paternity Leave balance is 2 days",
      trigger: '25'
    },
    {
      id: '25',
      user: true,
      trigger: '4'
    },
    {
      id: 'projectStatus',
      message: 'How can I help you with it?',
      trigger: 'projectStatusUser'
    },
    {
      id: 'projectStatusUser',
      user: true,
      trigger: '4'
    },
    {
      id: 'projectStatusTitle',
      message: 'Currently you are handling 5 projects. Choose a project to know the status update?',
      trigger: 'projectStatusOptions'
    },
    {
      id: 'projectStatusOptions',
      options: [
        { value: 1, label: 'No Guarantee Investment Bank', trigger: 'projectStatusValueMsg2' },
        { value: 2, label: '"No Moto" Research Tool', trigger: 'projectStatusValueMsg2' },
        { value: 3, label: 'Assessment Free Higher Education', trigger: 'projectStatusValueMsg2' },
        { value: 4, label: 'Invisible Mobile App', trigger: 'projectStatusValueMsg2' },
        { value: 5, label: 'Brainless Genius Website', trigger: 'projectStatusValueMsg2' },
      ],
    },
    {
      id: 'projectStatusValueMsg2',
      component: (
        <div>
          Current Sprint: 79
              <br />
          Sprint start date: 10/July/2017
              <br />
          Sprint end date: 24/July/2017
              <br />
          No blockers identified
              <br />
          Sprint burn down is in progress as per the plan, no deviations to highlight
            </div>
      ),
      trigger: 'projectStatusValueMsg3',
      asMessage: true
    },
    {
      id: 'projectStatusValueMsg3',
      user: true,
      trigger: '4'
    },
    {
      id: 'projectRiskTitle',
      component: (
        < div >
          Yes
              <br />
          Assessment Free Higher Education project has one open position for "L2" grade "Front-end developer". Which has to be closed before 30/August/2017
            </div >
      ),
      trigger: 'projectRiskOptions',
      asMessage: true
    },
    {
      id: 'projectRiskOptions',
      options: [
        { value: 1, label: 'JD for Front-end developer', trigger: 'projectRiskValue' },
        { value: 2, label: 'Identify suitable profiles from account resource pool', trigger: 'projectRiskValue' },
      ],
    },
    {
      id: 'projectRiskValue',
      component: (
        < div >
          Expertise level: L2
              <br />
          Primary skills: NodeJs, AngularJs, HTML5, CSS3
              <br />
          Secondary skills: SASS, Bootstrap, Typescript
              <br />
          Job activity: New module development, enhancement in the existing module, implementing responsiveness for the tool
              <br />
          Tentative project tenure: 8 to 12 months
            </div >
      ),
      trigger: 'projectRiskUser',
      asMessage: true
    },
    {
      id: 'projectRiskUser',
      user: true,
      trigger: '4'
    },
    {
      id: 'projectRiskResourcePool',
      message: "Sure",
      trigger: 'projectRiskResourcePoolMsg1'
    },
    {
      id: 'projectRiskResourcePoolMsg1',
      component: (
        <div>
          We have two profiles matching the given criteria
              <br />
          Adam Slander (Associate Id: 101010)
          <br />
          Ben Stiller (Associate Id: 200200)
            </div>
      ),
      trigger: 'projectRiskResourcePoolOptions',
      asMessage: true
    },
    {
      id: 'projectRiskResourcePoolOptions',
      options: [
        { value: 1, label: 'Schedule an online technical assessment', trigger: 'projectRiskResourcePoolValue' },
        { value: 2, label: 'Schedule a telephonic interview', trigger: 'projectRiskResourcePoolValue' },
        { value: 3, label: 'Schedule a face to face interview', trigger: 'projectRiskResourcePoolValue' },
      ],
    },
    {
      id: 'projectRiskResourcePoolValue',
      message: 'How can I help you with it?',
      trigger: 'projectRiskResourcePoolUser'
    },
    {
      id: 'projectRiskResourcePoolUser',
      user: true,
      trigger: '4'
    },
    {
      id: 'projectRiskResourceNotify',
      component: (
        <div>
          Notification sent to Project Lead Peter Paul (Associate Id: 412345).
              <br />
          You will be notified on all communications :)
          </div>
      ),
      asMessage: true,
      trigger: 'projectRiskResourceNotifyUser'
    },
    {
      id: 'projectRiskResourceNotifyUser',
      user: true,
      trigger: '4'
    },
    {
      id: 'scheduleTitle',
      component: (
        <div>
          1. You have "No Moto" Research Tool - Road map discussion with Charlotte and Will Smith from 3 PM to 4 PM
              <br />
          2. Risky Bank Investment Portal - budget planning meeting with Sundar and Gates from 4:30 PM to 5 PM
              <br />
          3. No more schedules are listed
            </div>
      ),
      trigger: 'scheduleUser',
      asMessage: true
    },
    {
      id: 'scheduleUser',
      user: true,
      trigger: '4'
    },
    {
      id: 'WelcomeText',
      message: 'You are welcome. Is there anything else I can do for you?',
      trigger: '3',
    },
    {
      id: 'Unknown',
      message: 'Can you say that again?',
      trigger: '3',
    },
    {
      id: 'LeaveApplyError',
      message: 'Unable to process you request. Can you please try again.',
      trigger: '14',
    },
  ],

};
