/* eslint-disable */

import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import ChatBot from 'react-simple-chatbot';
import { Data } from './steps.js';
export class MyOfficeBuddy extends Component {

  constructor(props) {
    super(props);
    if (sessionStorage.UserName && sessionStorage.UserName !== '') {
      let userName = sessionStorage.getItem('UserName');
      Data.steps[0].message += userName;
    } else {
      Data.steps[0].message = 'Hello';
    }
    this.state = {
      steps: Data.steps
    }
    this.logout = this.props.logout.bind(this);
  }

  render() {
    let bubbleStyle = {
      'border-radius' : '10px 10px 10px 0'
    }


    let footerStyle = {
      'borderTop': '1px solid #3a3f51'
    }

    const theme = {
      background: '#FFFFFF',
      headerBgColor: '#3A3F51',
      botBubbleColor: '#3A3F51',
      botFontColor: '#FFFFFF',
      userFontColor: '#FFFFFF',
      headerFontColor: '#FFFFFF',
      userBubbleColor: '#e6dddd',
      userFontColor: '#4a4a4a'
    }
    return (
      <div>
        <div className='LogoutContainer'>
          <a href='#' onClick={this.logout}>Logout</a>
        </div>
        <div className='ChatBotWrapper'>
          <ThemeProvider theme={theme}>
            <ChatBot className="chatBotContainer" headerTitle='MY OFFICE BUDDY'
              bubbleStyle={bubbleStyle}
              footerStyle={footerStyle}
              placeholder='Type your message'
              botAvatar='./asset/images/BotAvatar.png'
              userAvatar='./asset/images/UserAvatar.png'
              steps={this.state.steps} />
          </ThemeProvider>
          {/*<button type='button' onClick={this.handleClick}>Microphone</button>*/}
        </div>
      </div>
    );
  }
}
