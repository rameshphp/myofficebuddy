/* eslint-disable */

import React, { Component } from 'react';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.login = this.props.login.bind(this);
    }

    render() {
        return (
            <div className="wrapper">
                <div className="block-center mt-xl wd-xl">
                    <div className="panel panel-dark panel-flat">
                        <div className="panel-heading text-center">
                            <a href="javascript:;" className="login-app-title">
                                MY OFFICE BUDDY
                              </a>
                        </div>
                        <div className="panel-body">
                            <p className="text-center pv">SIGN IN TO CONTINUE</p>
                            <div className="form-group has-feedback">
                                <input id="email" type="email" placeholder="Enter email/phone/user name" required className="form-control" />
                                {/*<ul className="parsley-errors-list filled" id="parsley-id-email" >
                                    <li className="parsley-required">This value is required.</li>
                                </ul>*/}
                                {/*<span className="fa fa-envelope form-control-feedback text-muted"></span>*/}
                            </div>
                            <div className="form-group has-feedback">
                                <input id="password" type="password" placeholder="Password" required className="form-control" />
                                {/*<ul className="parsley-errors-list filled" id="parsley-id-password">
                                    <li className="parsley-required">This value is required.</li>
                                </ul>
                                <span className="fa fa-lock form-control-feedback text-muted"></span>*/}
                            </div>
                            <div className="clearfix">
                                <div className="checkbox c-checkbox pull-left mt0">
                                    <label>
                                        <input type="checkbox" value="" name="remember" />
                                        <span className="fa fa-check"></span>Remember Me</label>
                                </div>
                                <div className="pull-right"><a href="javascript:;" className="text-muted">Forgot your password?</a>
                                </div>
                            </div>
                            <button onClick={this.login} className="btn btn-block btn-primary mt-lg">Login</button>
                            <p className="pt-lg text-center">Need to Signup?</p><a href="javascript:;" className="btn btn-block btn-default">Register Now</a>
                        </div>
                    </div>
                </div>
                <div className="p-lg text-center">
                    <span>&copy;</span>
                    <span>2017</span>
                    <span>-</span>
                    <span>Unreal Imitators</span>
                    <br />
                    <span>CDB INT AI Hackathon Prototype</span>
                </div>
            </div>
        )
    }


}